defmodule Kleiner.ProcessorsTest do
  @moduledoc false

  use Kleiner.DataCase

  alias Kleiner.Processors
  alias Kleiner.Processors.SmallURL

  import Mox

  setup :verify_on_exit!

  @base_url "http://google.com"
  @valid_small_url_attrs %{"base_url" => @base_url, "slug" => ""}
  @invalid_small_url_attrs %{"base_url" => nil, "slug" => nil}

  describe "get_small_url/1" do
    test "given a valid slug, it returns {:ok, %SmallURL{}}" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      small_url = small_url_fixture()

      {:ok, result} = Processors.get_small_url(small_url.slug)
      assert small_url.base_url == result.base_url
    end

    test "given an invalid slug, returns {:error, nil}" do
      assert {:error, :not_found} = Processors.get_small_url("non_existing_slug")
    end
  end

  describe "create_small_url/1" do
    test "given valid data, it creates a small_url" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      assert {:ok, %SmallURL{} = small_url} = Processors.create_small_url(@valid_small_url_attrs)
      assert small_url.base_url == @base_url
      assert small_url.slug != ""
    end

    test "given valid data, including a slug, ensure that slug is used" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      custom_slug = "custom_slug"

      assert {:ok, %SmallURL{} = small_url} =
               Processors.create_small_url(%{"base_url" => @base_url, "slug" => custom_slug})

      assert small_url.base_url == @base_url
      assert small_url.slug == custom_slug
    end

    test "Remove spaces from custom slugs" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      custom_slug = "custom slug"

      assert {:ok, %SmallURL{} = small_url} =
               Processors.create_small_url(%{"base_url" => @base_url, "slug" => custom_slug})

      assert small_url.base_url == @base_url
      assert small_url.slug == "custom-slug"
    end

    test "if url already exists in database, and a slug isn't specified, return the same slug" do
      Kleiner.InetMock
      |> expect(:gethostbyname, fn _url -> {:ok, ""} end)
      |> expect(:gethostbyname, fn _url -> {:ok, ""} end)

      assert {:ok, %SmallURL{} = small_url} = Processors.create_small_url(@valid_small_url_attrs)
      assert small_url.base_url == @valid_small_url_attrs["base_url"]
      created_slug = small_url.slug

      assert {:ok, %SmallURL{} = duplicate} = Processors.create_small_url(@valid_small_url_attrs)
      assert duplicate.base_url == @valid_small_url_attrs["base_url"]
      assert duplicate.slug == created_slug
    end

    test "if url already exists in the database, but slug provided, create new slug" do
      Kleiner.InetMock
      |> expect(:gethostbyname, fn _url -> {:ok, ""} end)
      |> expect(:gethostbyname, fn _url -> {:ok, ""} end)

      assert {:ok, %SmallURL{} = small_url_1} =
               Processors.create_small_url(@valid_small_url_attrs)

      assert small_url_1.base_url == @base_url
      assert small_url_1.slug != ""

      custom_slug = "custom_slug"

      assert {:ok, %SmallURL{} = small_url_2} =
               Processors.create_small_url(%{"base_url" => @base_url, "slug" => custom_slug})

      assert small_url_2.base_url == @base_url
      assert small_url_1.slug != small_url_2.slug
      assert small_url_2.slug == custom_slug
    end

    test "create_small_url/1 with invalid data returns error tuple" do
      assert {:error, %Ecto.Changeset{}} = Processors.create_small_url(@invalid_small_url_attrs)
    end

    test "given an invalid url, return an error" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:error, ""} end
      )

      assert {:error, %Ecto.Changeset{}} =
               Processors.create_small_url(%{"base_url" => "http://aksdh.com"})
    end
  end

  @spec small_url_fixture(attrs :: map()) :: %SmallURL{}
  def small_url_fixture(attrs \\ %{}) do
    {:ok, small_url} =
      attrs
      |> Enum.into(@valid_small_url_attrs)
      |> Processors.create_small_url()

    small_url
  end
end
