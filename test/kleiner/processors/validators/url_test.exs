defmodule Kleiner.Processors.Validators.URLTest do
  @moduledoc false

  alias Kleiner.Processors.Validators.URL

  use ExUnit.Case

  import Mox

  require ExUnitProperties

  setup :verify_on_exit!

  describe "validate/1" do
    test "given a valid url, return the success tuple" do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      valid_url = url_generator()
      assert URL.validate(valid_url) == {:ok, valid_url}
    end

    test "given an invalid url, return the error tuple" do
      invalid_url = invalid_url_generator()
      assert URL.validate(invalid_url) == {:error, "invalid url"}
    end
  end

  @spec url_generator :: String.t()
  def url_generator do
    protocols = ["http://", "https://"]

    websites = [
      "google.com",
      "yahoo.com",
      "amazon.com",
      "www.google.com/search?q=url+shortener&oq=google+u&aqs=chrome.0.69i59j69i60l3j0j69i57.1069j0j7&sourceid=chrome&ie=UTF-8"
    ]

    stream_generator =
      ExUnitProperties.gen all name <- StreamData.member_of(websites),
                               protocol <- StreamData.member_of(protocols) do
        protocol <> name
      end

    stream_generator
    |> StreamData.resize(20)
    |> Enum.take(1)
    |> Enum.at(0)
  end

  @spec invalid_url_generator :: String.t()
  def invalid_url_generator do
    stream_generator =
      ExUnitProperties.gen all name <- StreamData.string(:alphanumeric),
                               name != "" do
        name <> ".com"
      end

    stream_generator
    |> StreamData.resize(20)
    |> Enum.take(1)
    |> Enum.at(0)
  end
end
