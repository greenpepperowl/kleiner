defmodule KleinerWeb.SmallURLControllerTest do
  @moduledoc false

  use KleinerWeb.ConnCase
  use PhoenixSwagger.SchemaTest, "priv/static/swagger.json"

  import Mox

  @base_url "http://google.com"
  @create_attrs %{"base_url" => @base_url, "slug" => ""}
  @invalid_attrs %{"base_url" => nil, "slug" => nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup :verify_on_exit!

  describe "create small_url" do
    test "renders small_url when data is valid", %{conn: conn, swagger_schema: schema} do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      response =
        conn
        |> post(Routes.small_url_path(conn, :create), small_url: @create_attrs)
        |> validate_resp_schema(schema, "SmallURLResponse")
        |> json_response(201)

      assert %{"base_url" => @base_url, "slug" => _slug} = response["data"]
    end

    test "renders changeset error when host is invalid", %{conn: conn, swagger_schema: schema} do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:error, :invalid} end
      )

      response =
        conn
        |> post(Routes.small_url_path(conn, :create), small_url: @create_attrs)
        |> validate_resp_schema(schema, "SmallURLInvalidResponse")
        |> json_response(422)

      assert %{"base_url" => ["invalid url"]} = response["errors"]
    end

    test "renders errors when data is invalid", %{conn: conn, swagger_schema: schema} do
      response =
        conn
        |> post(Routes.small_url_path(conn, :create), small_url: @invalid_attrs)
        |> validate_resp_schema(schema, "SmallURLInvalidResponse")
        |> json_response(400)

      assert response["errors"] != %{}
    end
  end

  describe "get small_url" do
    test "returns the small_url when the slug exists", %{conn: conn, swagger_schema: schema} do
      expect(
        Kleiner.InetMock,
        :gethostbyname,
        fn _url -> {:ok, ""} end
      )

      # Create the resource
      response =
        conn
        |> post(Routes.small_url_path(conn, :create), small_url: @create_attrs)
        |> validate_resp_schema(schema, "SmallURLResponse")
        |> json_response(201)

      assert %{"base_url" => @base_url, "slug" => slug} = response["data"]

      # Ensure that the same resource is returned given the slug
      response =
        conn
        |> get(Routes.small_url_path(conn, :get, slug))
        |> validate_resp_schema(schema, "SmallURLResponse")
        |> json_response(200)

      assert %{"base_url" => @base_url, "slug" => ^slug} = response["data"]
    end

    test "returns a 404 when the slug doesn't exists", %{conn: conn, swagger_schema: schema} do
      # conn = get(conn, Routes.small_url_path(conn, :get, "non_existing_slug"))

      response =
        conn
        |> get(Routes.small_url_path(conn, :get, "non_existing_slug"))
        |> validate_resp_schema(schema, "SmallURL404Response")
        |> json_response(404)

      assert response["errors"] != %{}
    end
  end
end
