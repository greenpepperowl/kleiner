defmodule KleinerWeb.SmallURLViewTest do
  use KleinerWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders a json object with the id removed" do
    base_url = "http://test_url"
    slug = "test_slug"

    assert render(KleinerWeb.SmallURLView, "show.json", %{
             small_url: %{base_url: base_url, slug: slug}
           }) == %{data: %{base_url: base_url, slug: slug}}
  end
end
