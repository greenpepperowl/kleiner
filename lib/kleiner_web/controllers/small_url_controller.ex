defmodule KleinerWeb.SmallURLController do
  @moduledoc """
  Controller to handle processing of SmallURLs
  """

  use KleinerWeb, :controller

  use PhoenixSwagger, except: [get: 1]

  alias Kleiner.Processors
  alias Kleiner.Processors.SmallURL

  action_fallback KleinerWeb.FallbackController

  @spec create(conn :: Plug.Conn.t(), attrs :: %{required(String.t()) => String.t()}) ::
          {:error, Ecto.Changeset.t()} | Plug.Conn.t()
  def create(conn, %{"small_url" => small_url_params}) do
    with {:ok, %SmallURL{} = small_url} <- Processors.create_small_url(small_url_params) do
      conn
      |> put_status(:created)
      |> render("show.json", small_url: small_url)
    end
  end

  @spec get(conn :: Plug.Conn.t(), attrs :: %{required(String.t()) => String.t()}) ::
          Plug.Conn.t() | {:error, :not_found}
  def get(conn, %{"slug" => slug}) do
    with {:ok, %SmallURL{} = small_url} <- Processors.get_small_url(slug) do
      render(conn, "show.json", small_url: small_url)
    end
  end

  # We test the definitions in the controller test for this module
  # coveralls-ignore-start
  swagger_path :create do
    post("/api/small_urls")
    description("Create a Small URL")

    parameters do
      small_url(:body, Schema.ref(:SmallURLCreate), "Object to be sent")
    end

    response(201, "Created", Schema.ref(:SmallURLResponse))
    response(422, "Invalid Data", Schema.ref(:SmallURLInvalidResponse))
  end

  swagger_path :get do
    PhoenixSwagger.Path.get("/api/small_urls/{slug}")
    description("Query a Small URL")

    parameters do
      slug(:path, :string, "Slug query")
    end

    response(200, "OK", Schema.ref(:SmallURLResponse))
    response(404, "Not Found", Schema.ref(:SmallURL404Response))
  end

  def swagger_definitions do
    %{
      SmallURLCreate:
        swagger_schema do
          title("Small URL Creation request")
          description("Request object to create a small url")

          properties do
            small_url(Schema.ref(:SmallURL))
          end
        end,
      SmallURLResponse:
        swagger_schema do
          title("Small URL response body")
          description("Response body for a created small url")

          properties do
            data(Schema.ref(:SmallURL))
          end
        end,
      SmallURLInvalidResponse:
        swagger_schema do
          title("Small URL invalid response body")
          description("Response body for a created small url")

          properties do
            errors(
              Schema.new do
                properties do
                  base_url(Schema.ref(:ErrorObject))
                  slug(Schema.ref(:ErrorObject))
                end
              end
            )
          end

          example(%{
            errors: %{
              base_url: ["can't be blank"],
              slug: ["can't be blank"]
            }
          })
        end,
      SmallURL:
        swagger_schema do
          title("Small URL")
          description("Parameters to create a shortened url")

          properties do
            base_url(:string, "The URL to be converted", required: true)
            slug(:string, "Either a user selected alias, or the blank string", required: true)
          end

          example(%{
            base_url: "http://google.com",
            slug: "google"
          })
        end,
      ErrorObject:
        swagger_schema do
          type(:array)
          items(%Schema{type: :string})
        end,
      SmallURL404Response:
        swagger_schema do
          title("Small URL 404 response")
          description("Repsonse returned with a 404 code")

          properties do
            error(:string, "The error")
          end

          example(%{
            error: %{
              detail: "Not Found"
            }
          })
        end
    }
  end

  # coveralls-ignore-stop
end
