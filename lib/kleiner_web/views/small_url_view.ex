defmodule KleinerWeb.SmallURLView do
  @moduledoc false

  use KleinerWeb, :view
  alias KleinerWeb.SmallURLView

  @spec render(template :: String.t(), attrs :: map()) :: map()
  def render("show.json", %{small_url: nil}) do
    %{data: %{}}
  end

  def render("show.json", %{small_url: small_url}) do
    %{data: render_one(small_url, SmallURLView, "small_url.json")}
  end

  def render("small_url.json", %{small_url: small_url}) do
    %{base_url: small_url.base_url, slug: small_url.slug}
  end
end
