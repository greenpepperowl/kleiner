defmodule KleinerWeb.Router do
  @moduledoc """
  The main router for the KleinerWeb application
  """

  use KleinerWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug PhoenixSwagger.Plug.Validate
  end

  scope "/api", KleinerWeb do
    pipe_through :api

    get "/small_urls/:slug", SmallURLController, :get
    post "/small_urls", SmallURLController, :create
  end

  # Enable swagger endpoint only for development
  if Mix.env() in [:dev, :test] do
    scope "/api/swagger" do
      forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :kleiner, swagger_file: "swagger.json"
    end
  end

  def swagger_info do
    %{
      schemes: ["http", "https"],
      info: %{
        version: "1.0",
        title: "Kleiner",
        description: "API Documentation for Kleiner v1",
        termsOfService: "Open for public",
        contact: %{
          name: "Andrew Stuart",
          email: "narayn60@gmail.com"
        }
      },
      consumes: ["application/json"],
      produces: ["application/json"],
      tags: [
        %{name: "SmallUrls", description: "SmallURL resources"}
      ]
    }
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: KleinerWeb.Telemetry
    end
  end
end
