defmodule Kleiner.Processors.SmallURL do
  @moduledoc """
  SmallURL changeset which holds the mapping between a given url
  and it's shortened slug
  """

  use Kleiner.Schema

  alias Kleiner.Processors.Validators.URL

  import Ecto.Changeset

  schema "small_urls" do
    field :base_url, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  @spec changeset(small_url :: %__MODULE__{}, attrs :: map()) :: Ecto.Changeset.t()
  def changeset(small_url, attrs) do
    small_url
    |> cast(attrs, [:base_url, :slug])
    |> validate_url()
    |> build_slug()
    |> unique_constraint(:slug, message: "Slug already exists")
    |> validate_required([:base_url, :slug])
  end

  defp build_slug(changeset) do
    case get_change(changeset, :slug) do
      nil ->
        slug = Ecto.UUID.autogenerate()
        put_change(changeset, :slug, slug)

      custom_slug ->
        custom_slug = String.replace(custom_slug, " ", "-")
        put_change(changeset, :slug, custom_slug)
    end
  end

  defp validate_url(changeset) do
    case get_field(changeset, :base_url) do
      nil ->
        changeset

      url ->
        case URL.validate(url) do
          {:ok, encoded_url} ->
            put_change(changeset, :base_url, encoded_url)

          {:error, "invalid url"} ->
            validate_change(changeset, :base_url, fn _, _url ->
              [base_url: "invalid url"]
            end)
        end
    end
  end
end
