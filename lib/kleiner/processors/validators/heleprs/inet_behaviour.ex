defmodule Kleiner.Processors.Validators.Helpers.InetBehaviour do
  @moduledoc """
  A behaviour to describe the basic inet behaviour to check that a hostname exists.

  This behaviour is defined to ensure that we can mock out external calls in the tests
  """
  @callback gethostbyname(any()) :: {:ok, any()} | {:error, any()}
end
