# Bypass testing as we're just calling out to a built in function
# coveralls-ignore-start
defmodule Kleiner.Processors.Validators.Helpers.Inet do
  @moduledoc """
  Module to determine if a given hotsname exists
  """
  @behaviour Kleiner.Processors.Validators.Helpers.InetBehaviour

  alias Kleiner.Processors.Validators.Helpers.InetBehaviour

  @impl InetBehaviour
  defdelegate gethostbyname(charlist), to: :inet
end

# coveralls-ignore-end
