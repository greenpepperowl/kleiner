defmodule Kleiner.Processors.Validators.URL do
  @moduledoc """
  Module to validate that a given string is a url
  """
  @behaviour Kleiner.Processors.Validators.ValidatorBehaviour

  alias Kleiner.Processors.Validators.ValidatorBehaviour

  @impl ValidatorBehaviour
  def validate(input_string) when is_binary(input_string) do
    encoded_url = URI.encode(input_string)

    if url?(encoded_url) do
      {:ok, encoded_url}
    else
      {:error, "invalid url"}
    end
  end

  def validate(_invalid_type), do: {:error, "invalid url"}

  @spec url?(input_string :: String.t()) :: boolean()
  defp url?(input_string) do
    url = URI.parse(input_string)
    url.scheme in ["http", "https"] && url.host =~ "." && valid_hostname?(url.host)
  end

  @spec valid_hostname?(hostname :: String.t()) :: boolean()
  defp valid_hostname?(hostname) do
    inet_module = Application.get_env(:kleiner, :inet_api)

    case inet_module.gethostbyname(Kernel.to_charlist(hostname)) do
      {:ok, _} -> true
      {:error, _} -> false
    end
  end
end
