defmodule Kleiner.Processors.Validators.ValidatorBehaviour do
  @moduledoc """
  Behaviour interface for a validator
  """

  @doc """
  Given an input, validate it against the specified rules
  """
  @callback validate(input :: any()) :: {:ok, any()} | {:error, String.t()}
end
