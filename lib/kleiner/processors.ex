defmodule Kleiner.Processors do
  @moduledoc """
  The Processors context.
  """

  import Ecto.Query, warn: false
  alias Kleiner.Repo

  alias Kleiner.Processors.SmallURL

  @type small_url_id :: String.t()

  @doc """
  Gets the base_url for a given slug
  """
  @spec get_small_url(slug :: String.t()) :: {:ok, %SmallURL{}} | {:error, :not_found}
  def get_small_url(slug) when is_binary(slug) do
    case Repo.get_by(SmallURL, slug: slug) do
      %SmallURL{} = small_url -> {:ok, small_url}
      nil -> {:error, :not_found}
    end
  end

  def get_small_url(_slut), do: {:error, :not_found}

  @doc """
  Given a base url, create a small_url. If the base url already exists in the DB, then
  just return the value that already exists.
  """
  @spec create_small_url(attrs :: %{required(String.t()) => String.t()}) ::
          {:ok, %SmallURL{}} | {:error, Ecto.Changeset.t()}
  def create_small_url(%{"base_url" => base_url, "slug" => slug} = attrs)
      when slug in ["", nil] do
    changeset = SmallURL.changeset(%SmallURL{}, attrs)

    if changeset.valid?() do
      case get_entry(base_url) do
        %SmallURL{} = small_url ->
          {:ok, small_url}

        nil ->
          Repo.insert(changeset)
      end
    else
      {:error, changeset}
    end
  end

  def create_small_url(attrs) do
    changeset = SmallURL.changeset(%SmallURL{}, attrs)
    Repo.insert(changeset)
  end

  @spec get_entry(base_url :: String.t()) :: %SmallURL{} | nil
  defp get_entry(base_url) do
    SmallURL
    |> where(base_url: ^base_url)
    |> limit(1)
    |> Repo.one()
  end
end
