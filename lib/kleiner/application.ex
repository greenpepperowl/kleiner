# credo:disable-for-this-file
defmodule Kleiner.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Kleiner.Repo,
      # Start the Telemetry supervisor
      KleinerWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Kleiner.PubSub},
      # Start the Endpoint (http/https)
      KleinerWeb.Endpoint
      # Start a worker by calling: Kleiner.Worker.start_link(arg)
      # {Kleiner.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Kleiner.Supervisor]

    # Parse the swagger file, so that we can use it in the plug to ensure that data
    # that we receive is valid
    PhoenixSwagger.Validator.parse_swagger_schema("priv/static/swagger.json")
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    KleinerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
