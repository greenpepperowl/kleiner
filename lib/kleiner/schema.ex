defmodule Kleiner.Schema do
  @moduledoc """
  Module to be used in-place of Ecto.Schema, to ensure
  that the id for a schema is a uuid. When using this module,
  ensure that the migration file adds an explicit id, and disables
  the primary_key.

  e.g
  Update

  ```
  create_table(:table) do
    ......
  ```

  to

  ```
  create_table(:table, primary_key: false) do
    add :id, :uuid, primary_key: true
    ....
  ```
  """
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      @primary_key {:id, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
      @derive {Phoenix.Param, key: :id}
    end
  end
end
