defmodule Kleiner.Repo do
  use Ecto.Repo,
    otp_app: :kleiner,
    adapter: Ecto.Adapters.Postgres

  @spec init(type :: any(), config :: keyword()) :: {:ok, keyword()}
  def init(_type, config) do
    if System.get_env("DATABASE_URL") != nil do
      {:ok, config}
    else
      config =
        config
        |> Keyword.put(:username, System.get_env("PGUSER", config[:username]))
        |> Keyword.put(:password, System.get_env("PGPASSWORD", config[:password]))
        |> Keyword.put(:database, System.get_env("PGDATABASE", config[:database]))
        |> Keyword.put(:hostname, System.get_env("PGHOST", config[:hostname]))
        |> Keyword.put(:port, "PGPORT" |> System.get_env(config[:port]) |> String.to_integer())

      {:ok, config}
    end
  end
end
