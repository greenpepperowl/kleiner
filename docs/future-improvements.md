# Future Improvements
## Testing Improvements
- Could introduce performance tests
- Could take advantage of property based Testing using StreamData

## Security Improvements
- Impvoe security checks during the CI/CD pipeline.

# Deployment
- Migrate away from Gigalixir to a self-managed k8s solution.
- Setup a proper DevOps pipeline to include things like environment variables and the likes

## General Improvements:
- Tidy up the Dockerfile to make better use of caching
- Come up with a nicer way of generating slugs, as UUIDs are kind of ugly for the end user
- Add complete error logging throughout the system, to make it easier to debug the application when on production
- Implement accounts similar to TinyURL
    + Link urls to accounts, and then make it possible to 
      change the base url for a given slug.
- If no future improvements are decided, remove the unused code paths generate by Phoenix (e.g socket in the KleinerWeb.Endpoint)
- Fix issues with Dialyzer on CI, as I don't believe it's properly caching at the moment which leads to longer pipeline times