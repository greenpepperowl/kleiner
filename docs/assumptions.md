# Assumptions made

This is the list of assumptions that I made when implementing this project

- If the same URL is given, it should just return the same slug, rather than creating a new one.
- Once a slug is created, there is no way (currently) of switching the base url.