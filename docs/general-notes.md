# General Notes

## CI/CD
- The project uses `gitlab-ci` to setup a CI/CD pipeline.

## Secrutiy
- CORS is setup to only allow connections from the specified Netlify app (or localhost).

## Deployment
- Currently the App is running on Gigalixir free tier, which has limited connections. In the future
  this would be migrated to a higher tier to ensure a higher load can be handled.