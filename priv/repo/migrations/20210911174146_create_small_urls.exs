defmodule Kleiner.Repo.Migrations.CreateSmallUrls do
  use Ecto.Migration

  def change do
    create table(:small_urls, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :base_url, :string
      add :slug, :string

      timestamps()
    end
  end
end
