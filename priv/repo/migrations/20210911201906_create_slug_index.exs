defmodule Kleiner.Repo.Migrations.CreateSlugIndex do
  use Ecto.Migration

  def change do
    create unique_index(:small_urls, [:slug])
  end
end
