# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :kleiner,
  ecto_repos: [Kleiner.Repo]

config :kleiner, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      # phoenix routes will be converted to swagger paths
      router: KleinerWeb.Router,
      # (optional) endpoint config used to set host, port and https schemes.
      endpoint: KleinerWeb.Endpoint
    ]
  }

# Configures the endpoint
config :kleiner, KleinerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "45BVInQOG13hPbvnhtskk+JrY/Es/nA9BIzeAZ+9ueplO5x4rNjaWpnOCwU7iSTF",
  render_errors: [view: KleinerWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Kleiner.PubSub,
  live_view: [signing_salt: "KSWBCoJK"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :phoenix_swagger, json_library: Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
