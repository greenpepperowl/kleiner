# Kleiner

**Backend to support Kleiner which enables users to shorten URLS** 

# Setup
There are 3 ways of running the project, our recommended setup is to use the `Makefile`. 

**NOTE**: If switching between build types (e.g Makefile -> local), you may need to remove the `_build` folder using `$ rm -rf _build`, due to how 
permissions are set by docker
## Makefile
To quickly run the application, please just use the provided Makefile. The only pre-requisite is to ensure that you have [docker](https://docs.docker.com/get-docker/) installed.

| Instructions | Command |
| - | - |
| Build the app | `$ make setup` |
| Run the app | `$ make server` |
| Run the tests | `$ make test` |
| Clean the docker images | `$ make clean` |

## ASDF

**Recommended if Developing**

If you'd like to run the project locally, it's recommended that you setup [asdf](https://github.com/asdf-vm/asdf), and [asdf-elixir](https://github.com/asdf-vm/asdf-elixir). 

Once these two are configured you should be able to run `asdf install` to get the correct versions of Elixir and Erlang.

You'll also need to have Postgres setup on your system. The required Postgres details can be found under `config/dev.exs`.

| Instructions | Command |
| - | - |
| Initial configuration | `$ mix setup` |
| Run the server | `$ mix phx.server` |
| Run the server with interactive REPL | `$ iex -S mix phx.server` |
| Run all tests | `$ MIX_ENV=test mix test` |
| Run the linter | `$ mix lint` |
| Run Dialyzer | `$ mix dialyzer` |
| Generate Docs | `$ mix docs` |
| Generate coverage report | `$ MIX_ENV=test mix ci.coverage` |
| Generate HTML coverage report | `$ MIX_ENV=test mix coveralls.html` |

## Docker
If you don't want to set anything up locally, you can also take advantage of the docker images.

| Instructions | Command |
| - | - |
| Configuration | `$ docker-compose build` |
| Run the application | `$ docker-compose up phoenix db` |
| Run all tests | `$ docker-compose up test` |
| Clean all images | `$ docker-compose rm` |

# Coverage
The project uses [Coveralls](https://github.com/parroty/excoveralls) to generate coverage. Follow the guide there to get more information on how to generate
more advanced coverage reports (e.g HTML).


# Project Documentation
You can generate the hex doc documentation for the project by running `$ mix docs`. Once this is run, you should be able to open up the docs from `doc/index.html`.
# API Documentation

The project uses [Phoenix Swagger](https://github.com/xerions/phoenix_swagger) to generate API documentation. By using swagger we are able to get a detailed description of each endpoint, as well as easily seeing the expected params and responses. 

To access the documentation, please navigate to `localhost:4000/api/swagger` when you have the application running.

# Additional Notes
- [Future Improvements](./docs/future-improvements.md)
- [Assumpetions](./docs/assumptions.md)
- [General Notes](./docs/general-notes.md)
